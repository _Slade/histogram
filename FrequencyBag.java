public class FrequencyBag<T>
{
    private LinkedList<PairFreq<T>> list;
    private int sampleSize;
    private int uniqueEntries;
    /**
     * Constructor
     * Constructs an empty frequency bag.
     */
    public FrequencyBag()
    {
        this.list          = new LinkedList<PairFreq<T>>();
        this.sampleSize    = 0;
        this.uniqueEntries = 0;
    }

    /**
     * Adds new entry into this frequency bag.
     * @param datum The datum to be added into this frequency bag.
     */
    public void add(T datum)
    {
        int find = this.contains(datum);
        if (find != -1)
        {
            this.list.get(find).incFreq();
        }
        else
        {
            this.list.add(new PairFreq<T>(datum));
            ++this.uniqueEntries;
        }
        ++this.sampleSize;
    }

    /**
     * Searches list for the item
     * @return Index at which item was found, -1 otherwise
     */
    private int contains(T item)
    {
        if (this.list.isEmpty())
            return -1;
        Iterator<PairFreq<T>> iter = this.list.iterator();
        int index = 0;
        while (iter.hasNext())
        {
            PairFreq pair = iter.next();
            /* System.out.println(pair.item().toString() + " " + item.toString()); */
            if (pair.item().equals(item))
                return index;
                
            ++index;
        }
        return -1;
    }

    /**
     * Gets the number of occurrences of {@code datum} in this frequency bag.
     * @param datum the data to be checked for its number of occurrences.
     * @return the number of occurrences of {@code datum} in this frequency bag.
     */
    public int getFrequencyOf(T datum)
    {
        if (this.uniqueEntries == 0) { return 0; }
        int index = this.contains(datum);
        if (index == -1) { return 0; }
        return this.list.get(index).freq();
    }

    /**
     * Gets the maximum number of occurrences in this frequency bag.
     * @return the maximum number of occurrences of an entry in this
     * frequency bag.
     */
    public int getMaxFrequency()
    {
        int maxFreq = 0;
        Iterator<PairFreq<T>> iter = this.list.iterator();
        while (iter.hasNext())
        {
            Integer freq = iter.next().freq();
            if (freq > maxFreq)
                maxFreq = freq;
        }
        return maxFreq;
    }

    /**
     * Gets the probability of {@code datum}
     * @param datum the specific datum to get its probability.
     * @return the probability of {@code datum}
     */
    public double getProbabilityOf(T datum)
    {
        return ((double) this.getFrequencyOf(datum)) / this.sampleSize;
    }

    /**
     * Empty this bag.
     */
    public void clear()
    {
        this.list = new LinkedList<PairFreq<T>>();
        this.sampleSize = this.uniqueEntries = 0;
    }

    /**
     * Gets the number of entries in this bag.
     * @return the number of entries in this bag.
     */
    public int size()
    {
        return this.sampleSize;
    }

    public String toString()
    {
        return this.list.toString();
    }
} // end FrequencyBag
