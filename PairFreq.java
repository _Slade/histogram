public class PairFreq<T1>
{
    private T1 item;
    private int frequency;

    public PairFreq(T1 arg1)
    {
        this.item = arg1;
        this.frequency = 1;
    }

    public T1 item()
    {
        return item;
    }

    public int freq()
    {
        return frequency;
    }

    public void incFreq()
    {
        ++this.frequency;
    }

    public String toString()
    {
        return item + ": " + frequency;
    }
}
