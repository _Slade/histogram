public class Test
{
    public static void main (String[] args)
    {
        /* Normal use */
        {
            LinkedList<String> ll = new LinkedList<String>("First");
            System.out.println("ok - single-element constructor");

            assert ll.get(0).equals("First");
            System.out.println("ok - first element equals \"First\"");

            for (int i = 2; i < 6; ++i) { ll.add(String.format("%d", i * 10)); }

            assert ll.getLength() == 5;
            System.out.println("ok - length == 5");
            System.out.println(ll);

            ll.add(3, "Insert at index");
            assert ll.getLength() == 6;
            assert ll.get(3).equals("Insert at index");
            System.out.println("ok - Insert at index 3");
            System.out.println(ll);

            ll.remove(3);
            assert ll.getLength() == 5;
            System.out.println(ll);

            ll.remove(0);
            assert ll.getLength() == 4;
            System.out.println(ll);
            System.out.println("ok - remove index 0");

            ll.prepend("New first");
            assert ll.get(0).equals("New first");
            System.out.println("ok - prepend");
        }
        /* Singleton */
        {
            LinkedList<String> singleton = new LinkedList<String>("One");
            assert singleton.getLength() == 1;
            System.out.println("ok - singleton constructor");

            singleton.remove(0);
            assert singleton.getLength() == 0;
            System.out.println(singleton);
            System.out.println("ok - remove(0) on singleton");

            singleton.add("Foo");
            assert singleton.getLength() == 1;
            System.out.println(singleton);
            System.out.println("ok - add element to emptied singleton");
        }
        /* Empty constructor */
        {
            LinkedList<String> empty = new LinkedList<String>();
            assert empty.getLength() == 0;
            assert empty.isEmpty();
            System.out.println("ok - construct empty list");

            try { empty.remove(0); }
            catch (IndexOutOfBoundsException e)
            {
                System.out.println("Caught intentional exception: " + e);
            }
        }
        /* Iterator */
        {
            System.out.println("ok - iterator");
            LinkedList<Integer> foo = new LinkedList<Integer>();            
            for (int i = 1; i < 11; ++i)
                foo.add(i * 10);
            Iterator<Integer> iter = foo.iterator();

            while (iter.hasNext())
                System.out.print(iter.next() + " ");
            System.out.println("ok - iter returns all elements");
            assert !iter.hasNext();
            System.out.println("ok - iter.hasNext() is false");
        }
    }
}
