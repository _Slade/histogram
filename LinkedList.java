public class LinkedList<T>
{
    private int length;
    /* Cache the first and last nodes for quick access */
    private Node<T> firstNode;
    private Node<T> lastNode;

    /* Constructor */
    public LinkedList()
    {
        this.length = 0;
    }

    public LinkedList(T datum)
    {
        this.firstNode = new Node<T>(datum);
        // Since we only allow initialization with one element
        this.lastNode  = this.firstNode;
        this.length = 1;
    }

    private Node<T> getNode(int index)
    {
        if (index >= this.length || index < 0)
        {
            throw new IndexOutOfBoundsException(
                "Illegal index: " + index + " in "
                + this.length + " element list"
            );
        }
        if (index == 0) { return this.firstNode; }
        Node<T> curNode = this.firstNode;
        for (int i = 0; i < index; ++i) { curNode = curNode.next; }
        return curNode;
    }

    /* Without an index, appends the element */
    public boolean add(T datum)
    {
        Node<T> newNode = new Node<T>(datum);
        if (this.length == 0)
        {
            this.firstNode = newNode;
            this.lastNode  = newNode;
        }
        else
        {
            this.lastNode.next = newNode;
            /* Keep track of the new last node */
            this.lastNode = newNode;
        }
        ++this.length;
        return true;
    }

    /* Inserts a new node AFTER the node at the given index */
    public boolean add(int index, T newNode)
    {
        if (index == this.length - 1) { return add(newNode); }
        Node<T> prevNode = this.getNode(index - 1);
        prevNode.next = new Node<T>(newNode, prevNode.next);
        ++this.length;
        return true;
    }


    /* Add to the beginning of the list */
    public boolean prepend(T datum)
    {
        Node<T> newFirst  = new Node<T>(datum, this.firstNode);
        this.firstNode = newFirst;
        ++this.length;
        return true;
    }

    public boolean remove(int index)
    {
        if (this.length == 0)
        {
            throw new IndexOutOfBoundsException(
                "Cannot remove element from empty list"
            );
        }
        if (index == 0)
        {
            this.firstNode = this.firstNode.next;
        }
        else
        {
            Node<T> prevNode = this.getNode(index - 1);
            prevNode.next = prevNode.next.next;
        }
        --this.length;
        return true;
    }

    /** Gets the datum at the specified index
     * @param int index at which to get the object
     * @return The T reference at the given index
     */
    public T get(int index)
    {
        return getNode(index).datum;
    }

    public int getLength()
    {
        return this.length;
    }

    public boolean isEmpty()
    {
        return this.length == 0;
    }

    public void clear()
    {
        /* This should kick in GC for the rest of the nodes */
        this.firstNode = null;
        this.lastNode  = null; // Don't think I need this, but whatever.
        this.length    = 0;
    }

    public Iterator<T> iterator()
    {
        return new LLIterator<T>(this.firstNode);
    }

    public String toString()
    {
        if (this.length == 0) { return "[]"; }
        if (this.length == 1) { return "[" + this.firstNode.datum + "]"; }
        String str = "[" + this.firstNode + "]->";
        Node<T> curNode = firstNode;
        while ( (curNode = curNode.next) != null
                && curNode != this.lastNode )
        {
            str += ("[" + curNode + "]->");
        }
        return str += "[" + this.lastNode + "]";
    }

    class Node<T>
    {
        // XXX Maybe make Node an immutable class
        private T datum;
        private Node<T> next;

        public Node(T newDatum)
        {
            this.datum = newDatum;
            this.next  = null;
        }

        public Node(T newDatum, Node<T> nextNode)
        {
            this.datum = newDatum;
            this.next  = nextNode;
        }

        public String toString()
        {
            if (this.datum == null) { return "null"; }
            return this.datum.toString();
        }
    } // end Node
    class LLIterator<T> implements Iterator<T>
    {
        Node<T> curNode;

        public LLIterator(Node<T> first)
        {
            this.curNode = first;    
        }

        public boolean hasNext()
        {
            return this.curNode != null;
        }

        public T next()
        {
            T retval = this.curNode.datum;
            this.curNode  = this.curNode.next;
            return retval;
        }
    } // end Iterator
} // end LinkedList
